import { defineStore } from "pinia";
import { useUserStore } from '@/stores/user' 

export const usePostsStore = defineStore("posts", {
  state: () => {
    return {
      posts: []
    }
  },
  actions: {
    async fetchPosts() {
      const user = useUserStore()

      if (!user.bearer_token) {
        return null
      }

      const bearer = user.bearer_token

      const res = await fetch('http://127.0.0.1:3000/posts', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + bearer
        }
      })

      const response_json = await res.json()
      this.posts = response_json.posts
    }
  }
});
