import { defineStore } from "pinia";

export const useUserStore = defineStore("user", {
  state: () => {
    return { 
      email: '',
      bearer_token: ''
    };
  },
  actions: {
    async loginUser(email, password) {
      const user_data = JSON.stringify({
        user: {
          email,
          password
        }
      })

      const res = await fetch('http://127.0.0.1:3000/users/sign_in', {
        method: 'POST',
        body: user_data,
        headers: {
          'Content-Type': 'application/json'
        }
      })

      const user = await res.json()
      const bearer = res.headers.get('authorization').substring('Bearer '.length)
      
      this.updateUser(user.email, bearer)
    },
    updateUser(email, bearer) {
      this.email = email
      this.bearer_token = bearer
    }
  }
});
