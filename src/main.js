import { createPinia } from 'pinia'
import { defineCustomElement } from './define_custom_element'
import PostsApp from './PostsApp.ce.vue'
import router from './router'

const pinia = createPinia()

const PostsAppElement = defineCustomElement(PostsApp, {
  plugins: [router, pinia],
})

customElements.define('posts-app-element', PostsAppElement)
